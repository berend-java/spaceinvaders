package spaceinvaders;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;

import engine.*;
import spaceinvaders.Set;

public class SpaceInvaders extends Game {
	
	
	public int PLAYER_START_X 		= 50;	
	public int PLAYER_START_Y 		= 700;
	public int PLAYER_WIDTH 		= 50; 
	public int PLAYER_HEIGHT 		= 20;
	public int PLAYER_SPEED 		= 15; 
	
	public int MONSTER_START_X 		= 50; 
	public int MONSTER_START_Y 		= 100; 
	public int MOVE 				= 0;
	public int MAX_MOVE_WIDTH		= 0;
	public int MIN_MOVE_WIDTH		= 0;
	
	public int BLOCK_SIZE 			= 25;
	public int BLOCKS 				= 5;
	
	public boolean IS_SHOT			= false;
	public boolean IS_FIRST_SHOT 	= false;
	public int BULLET_WIDTH 		= 3; 
	public int BULLET_HEIGHT 		= 15; 	
	public int BULLET_X				= 0;
	public int BULLET_Y				= 0;
	
	public int GAME_OVER 			= 500;
	
	public boolean FIRST_WRITE		= true;	
	
	
	public SpaceInvaders() {
		title = "Space Invaders";
		height = 800;
		width = 1000;
	}
	
	public static void main(String arg[]) {
		GameApplication.start(new SpaceInvaders());
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == 37 && PLAYER_START_X > 10) {
			moveLeft();
		} 
		else if(e.getKeyCode() == 39 && PLAYER_START_Y < 920) {
			moveRight();
		}
		else if(e.getKeyCode() == 32) {
			if(!IS_SHOT) {
				shoot();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {}
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void playerUpdate() {
		
	}
	
	@Override
	public void update() {
		calcMax();
		
		if(MONSTER_START_X >= GAME_OVER) { 
			GameApplication.gameOver();
			gameOver();
			MONSTER_START_X = 50;
			MONSTER_START_Y = 100;	
		}
		
		if(MONSTER_START_Y >= MAX_MOVE_WIDTH) { 
			MOVE = 1; 
			MONSTER_START_X+=10;
			delay-=10;
		} 
		else if(MONSTER_START_Y <= MIN_MOVE_WIDTH) {
			MOVE = 0;
			MONSTER_START_X+=10;
			delay-=10;
		}
		
		if(MOVE == 1) {
			MONSTER_START_Y -= Monster.getMonsterFallSpeed();
		} else if (MOVE == 0) {
			MONSTER_START_Y += Monster.getMonsterFallSpeed();
		}
	}
	
	@Override
	public void draw(Graphics2D g) {
		g.setColor(Color.WHITE);
		g.fillRect(PLAYER_START_X, PLAYER_START_Y, PLAYER_WIDTH, PLAYER_HEIGHT);
		
		if(IS_SHOT) {
			if(IS_FIRST_SHOT) {
				BULLET_X = PLAYER_START_X+20;
				BULLET_Y = PLAYER_START_Y-5;
				IS_FIRST_SHOT = false;
			}
			g.fillRect(BULLET_X, BULLET_Y, BULLET_WIDTH, BULLET_HEIGHT);
			BULLET_Y-=2;	
			
			for(Monster m: Monster.getMonsters()) {
				int monsterS = Monster.getMonsterSize()+3;
				int mx = m.getX()-5;
				if(BULLET_X < m.getY()+monsterS && BULLET_X > m.getY() && BULLET_Y > mx && BULLET_Y < m.getX()+monsterS) {
					if(m.isAlive()) {
						m.die();
						IS_SHOT = false;
						IS_FIRST_SHOT = false;
					}
				}
			}
			
			if(BULLET_Y < 0) {
				IS_SHOT 		= false;
				IS_FIRST_SHOT 	= false;
			}
		}
		
		int saveY = MONSTER_START_Y;
		int saveX = MONSTER_START_X;
		int regel = 0;
		int count = 0;
		int id = 0;
		
		for(int i = 0; i < Monster.getCols(); i++) {
			for(int ii = 0; ii < Monster.getRows(); ii++) {
				if(count >= Monster.getRows()) {
					regel++;
					count = 0;
				}
				if(FIRST_WRITE) {
					new Monster(1, regel, MONSTER_START_Y, MONSTER_START_X, id);
					count++;
				}
				else {
					Monster m = Monster.getMonster(id);
					m.setX(MONSTER_START_X);
					m.setY(MONSTER_START_Y);
					
					if(m.isAlive()) {
						g.fillRect(m.getY(), m.getX(), Monster.getMonsterSize(), Monster.getMonsterSize());
					}
				}
				id++;
				MONSTER_START_Y+=60;
			}
			MONSTER_START_Y = saveY;
			MONSTER_START_X+=60;
		}
		MONSTER_START_X = saveX;
		IS_FIRST_SHOT = false;
	
		/*int t = 150;
		for(int i = 0; i < blocks; i++) {
			g.fillRect(t, 500, blokSize, blokSize);
			t+=150;
		} */	
	}
	public void shoot() {
		IS_SHOT = true;
		IS_FIRST_SHOT = true;
	}
	
	public void calcMax() {
		MAX_MOVE_WIDTH = Monster.rechtseRegel();
		MIN_MOVE_WIDTH = Monster.linkseRegel();
	}
	
	public void moveLeft() {
		PLAYER_START_X-=PLAYER_SPEED;
	}
	public void moveRight() {
		PLAYER_START_X+=PLAYER_SPEED;
	}
}
