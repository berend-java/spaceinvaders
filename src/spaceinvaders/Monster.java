package spaceinvaders;

import java.util.ArrayList;

public class Monster {
	private int level, monsterRow, x, y;
	
	private static ArrayList<Monster> monsters = new ArrayList<Monster>();	// lijst met monsters
	private boolean isAlive = true;
	private int id;
	private static int monsterSize = 40;									// Monster size
	private static int monsterSpeed = 1;									// Monster speed
	private static int cols = 5,  rows = 8;									// Aantal regels monsters, aantal monsters per regels
	private static int monsterFallSpeed = 10;								// Snelheid dat monsters dalen

	
	public Monster(int l, int mR, int xx,  int yy, int i) {
		level = l;
		x = xx;
		y = yy;
		monsterRow = mR;
		monsters.add(this);
		id = i;
	}
	
	public void die() {
		isAlive = false;
	}
	public static int rechtseRegel() {
		int t = 0;
		for(Monster m: monsters) {
			if(m.getMonsterRow() > t) {
				t = m.getMonsterRow();
			}
		}
		int r = 0;
		
		switch(t) {
			case 0: r = 900; break;
			case 1: r = 830; break;
			case 2: r = 780; break;
			case 3: r = 720; break;
			case 4: r = 680; break;
			case 5: r = 610; break;
			case 6: r = 550; break;
			case 7: r = 500; break;
			case 8: r = 450; break;
		}
		return r;
	}
	
	public static int linkseRegel() {
		int t = 9;
		for(Monster m: monsters) {
			if(m.getMonsterRow() < t) {
				t = m.getMonsterRow();
			}
		}
		
		int l = 0;
		switch(t) {
			case 0: l = 10; break;
			case 1: l = 50; break;
			case 2: l = 100; break;
			case 3: l = 150; break;
			case 4: l = 200; break;
			case 5: l = 250; break;
			case 6: l = 300; break;
			case 7: l = 350; break;
			case 8: l = 400; break;
		}
		return l;
	}
	
	public int aantalMonsters() {
		return monsters.size();
	}
	
	public static Monster getMonster(int id) {
		Monster monster = null;
		for(Monster m: monsters) {
			if(m.getId() == id) {
				monster = m;
			}
		}
		return monster;
	}
	
	public int getId() {
		return id;
	}
	public int getLevel() {
		return level;
	}
	
	public int getY() {
		return y;
	}
	
	public int getX() {
		return x;
	}
	
	public static ArrayList<Monster> getMonsters() {
		return monsters;
	}

	public static int getMonsterSize() {
		return monsterSize;
	}

	public static void setMonsterSize(int monsterSize) {
		Monster.monsterSize = monsterSize;
	}

	public static int getCols() {
		return cols;
	}

	public static void setCols(int cols) {
		Monster.cols = cols;
	}

	public static int getRows() {
		return rows;
	}

	public static void setRows(int rows) {
		Monster.rows = rows;
	}

	public static int getMonsterSpeed() {
		return monsterSpeed;
	}

	public static void setMonsterSpeed(int monsterSpeed) {
		Monster.monsterSpeed = monsterSpeed;
	}

	public static int getMonsterFallSpeed() {
		return monsterFallSpeed;
	}

	public static void setMonsterFallSpeed(int monsterFallSpeed) {
		Monster.monsterFallSpeed = monsterFallSpeed;
	}

	public int getMonsterRow() {
		return monsterRow;
	}

	public void setMonsterRow(int monsterRow) {
		this.monsterRow = monsterRow;
	}
	
	public String toString() {
		String s = "Monster van level: " + 1 + " staat op rij " + monsterRow + " om precies te zijn  op: " + x + ":" + y;
		return s;
	}

	public static boolean isAlive(int id) {
		boolean b = false;
		for(Monster m: monsters) {
			if(m.getId() == id) {
				if(m.isAlive()) {
					b = true;
				}
			}
		}
		return b;
	}

	public boolean isAlive() {
		return isAlive;
	}
	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}
	
	public void setX(int xx) {
		x = xx;
	}
	
	public void setY(int yy) {
		y = yy;
	}
}
