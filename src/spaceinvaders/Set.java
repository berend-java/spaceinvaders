package spaceinvaders;

/**
 * @author Berend de Groot
 * Note: Changing these settings is only meant for those who know what they are doing.
 * If you don't, leave the settings as they are.
 */

/*
 * Vars final maken, vars die aangepast worden inlezen in de juiste class en daarna daar aanpassen. Interface nooit aanpassen.
 */
public class Set {
	public int PLAYER_START_X 		= 50;				// Start position player (x).
	public int PLAYER_START_Y 		= 700;				// Start position player (y).
	public int PLAYER_WIDTH 		= 50; 				// Player width.
	public int PLAYER_HEIGHT 		= 20;				// Player height.
	public int PLAYER_SPEED 		= 15; 				// Player speed.
	
	public int MONSTER_START_X 		= 50; 				// Start position first monster (x).
	public int MONSTER_START_Y 		= 100; 				// Start position first monster (y).
	public int MOVE 				= 0; 				// Monster movement (0 = right, 1 = left).
	public int MAX_MOVE_WIDTH		= 0;				// Max available movement (0 by default, will be replaced later).
	public int MIN_MOVE_WIDTH		= 0;				// Minimum available movement (always 0, yet not hardcoded).
	
	public int BLOCK_SIZE 			= 25; 				// Size of defense blocks.
	public int BLOCKS 				= 5; 				// Amount of defense blocks.
	
	public boolean IS_SHOT			= false;			// Determine whether the player is shooting or not.
	public boolean IS_FIRST_SHOT 	= false; 			// Determine whether its the first shot or not. (Meaning; the same loop as the player pressed "SPACE").
	public int BULLET_WIDTH 		= 3; 				// Bullet width.
	public int BULLET_HEIGHT 		= 15; 				// Bullet height.
	public int BULLET_X				= 0;				// Bullet X position (not known yet since space triggers a shot).
	public int BULLET_Y				= 0;				// Bullet Y position (not known yet since space triggers a shot).
	
	public int GAME_OVER 			= 500;				// Game over when monster will be this much pixels from the top.
	
	public boolean FIRST_WRITE		= true;				// Determine whether its the first time the monsters are being written.
}
