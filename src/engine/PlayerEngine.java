package engine;


public class PlayerEngine extends Thread {
	private boolean paused;
	Game game;
	Canvas canvas;

	public PlayerEngine(Game g, Canvas c) {
		super();
		game = g;
		canvas = c;
		paused = false;
	}
	
	public void run() {
		while(!game.isOver()) {
			if(!paused) {
				//game.playerUpdate();
				canvas.repaint();
			}
			try {
				PlayerEngine.sleep(game.getPlayerDelay());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void pauseGame() {
		paused = true;
	}
	
	public void resumeGame() {
		paused = false;
	}
}