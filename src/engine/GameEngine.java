package engine;

public class GameEngine extends Thread {
	private boolean paused;
	Game game;
	Canvas canvas;

	public GameEngine(Game g, Canvas c) {
		super();
		game = g;
		canvas = c;
		paused = false;
	}
	
	public void run() {
		while(!game.isOver()) {
			if(!paused) {
				game.update();
				canvas.repaint();
			}
			try {
				GameEngine.sleep(game.getDelay());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void pauseGame() {
		paused = true;
	}
	
	public void resumeGame() {
		paused = false;
	}
}
