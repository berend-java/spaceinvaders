package engine;

import java.awt.Graphics2D;
import java.awt.event.KeyListener;

public abstract class Game implements KeyListener {
	protected boolean isOver;
	protected String title = "My Game";
	protected int height = 300, width = 300, delay = 300, playerDelay = 1;
	
	
	public Game() {
		isOver = false;
	}
	
	public boolean isOver() {
		return isOver;
	}
	
	public void gameOver() {
		isOver = true;
	}

	public abstract void playerUpdate();
	public abstract void update();
	public abstract void draw(Graphics2D g);
	
	public String getTitle() { return title; }
	public int getWidth() { return width; }
	public int getHeight() { return height; }
	public int getDelay() { return delay; }
	public int getPlayerDelay() { return playerDelay; }
	public void resize(int width, int height) {}
	

}
