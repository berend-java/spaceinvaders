package engine;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class GameApplication {
	public static JFrame frame;
	
	public static void start(final Game game) {
		SwingUtilities.invokeLater(new Runnable(){
			@Override 
			public void run() {
				frame = new JFrame(game.getTitle());
				frame.setSize(game.getWidth(), game.getHeight());
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getContentPane().setBackground(Color.BLACK);
				
		    	Canvas c = new Canvas(game);
		    	frame.add(c);
		    	
		    	frame.setVisible(true);
		    	c.requestFocus();
		    	
		    	GameEngine e = new GameEngine(game, c);
		    	PlayerEngine p = new PlayerEngine(game, c);
		    	e.start();
		    	p.start();
			}
		});
	}
	
	public static void gameOver() {
		JOptionPane.showMessageDialog(frame, "Game over, fail.");
	}
}